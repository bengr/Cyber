#!/bin/bash

echo "***Installing netaadr***"
cd netaddr-0.7.14
python setup.py install
cd ..

echo "***Installing scapy***"
cd "scapy-2.3.1"
python setup.py install
cd ..

echo "***installing sh***"
cd "sh-1.11"
python setup.py install
