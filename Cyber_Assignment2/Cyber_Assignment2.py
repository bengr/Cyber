__author__ = "Ben Grynhaus"
__doc__ == "A tool to perform a TCP scan on a Subnet, a DNS Amplification attack or a SYN-Flood attack.\nHaving 'scrapy', 'netaddr' and 'sh' libraries installed is a must to run this tool.\n\tNOTE: This tool only works on posix-based systems."

import sys
import optparse
from cyber_utils import cyber_utils

##FOR DEBUG USE ONLY##
#scan_tcp
#sys.argv.append("-f scan_tcp")
#sys.argv.append("10.0.0.138")
#sys.argv.append("255.255.255.0")

#dns_amp_attack
#sys.argv.append("-f dns_amp")
#sys.argv.append("192.168.40.129")
#sys.argv.append("8000")

#syn_flood_attack
#sys.argv.append("-f syn_flood")
#sys.argv.append("10.0.0.9")
#sys.argv.append("8000")

def main():
	valid_functions = ["scan_tcp", "dns_amp", "syn_flood"]

	options = {}
	parser = optparse.OptionParser("usage: %prog -f scan_tcp|dns_amp|syn_flood [args].\n\targs are dependend on function to execute.\n\t\tscan_tcp: [subnet_ip] [subnet_mask] (i.e 10.0.0.138 255.255.255.0)\n\t\tdns_amp: [ip] [port] (optional: [base_dns_server] (i.e google.com) [send_limit] (i.e 500))\n\t\tsyn_flood: [ip] [port] (optional: [packets_to-send] (i.e 500))")
	parser.add_option("-f", "--func", dest="func", type="string", help="specify a function to execute")

	(options, args) = parser.parse_args()
	func = str(options.func).strip()

	if func not in valid_functions:
		print parser.usage
		sys.exit(1)

	if func == "scan_tcp":
		print "**SCAN TCP**"
		cyber_utils.tcp_scan(args)
	elif func == "dns_amp":
		print "**DNS amplification attack**"
		cyber_utils.dns_amplification_attack(args)
	elif func == "syn_flood":
		print "**SYN-flood attack**"
		cyber_utils.syn_flood_attack(args)
	
	print "**FINISHED**"

if __name__ == '__main__':
	main()