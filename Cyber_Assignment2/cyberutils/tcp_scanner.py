__author__ = "Ben Grynhaus"

from __builtin__ import staticmethod
from socket import *

class tcp_scanner(object):

	@staticmethod
	def scanner(host, ports):
		""" scans a given host on given ports """

		try:
			ip = gethostbyname(host)
		except:
			print("unable to resolve '%s': Unknown host" % host)
			return

		try:
			targetName = gethostbyaddr(ip)
			print("Scanning target " + targetName[0])
		except:
			print("Scanning target " + ip)

		for port in ports:
			print ("Scanning port %d" % port)
			tcp_scanner.__connect__(host, int(port))

	@staticmethod
	def __connect__(host ,port):
		try:
			connSocket = socket(AF_INET, SOCK_STREAM)
			connSocket.settimeout(1)

			connSocket.connect((host, port))
			connSocket.close()
			print ("%d/TCP port open" % port)
		except:
			print ("%d/TCP port closed" % port)
