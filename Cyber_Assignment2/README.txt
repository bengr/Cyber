# Prerequisites
1. You must have 'scapy' (https://pypi.python.org/pypi/scapy/2.3.1), 'netaddr' (https://pypi.python.org/pypi/netaddr) and 'sh' (https://pypi.python.org/pypi/sh) installed to use the tool.
2. (Recommended) Run the install_all.sh script in Prerequisites to easily install all required libraries.

# Usage
1. Run the Cyber_Assignment2.py file and see usage when prompted.